# Série: Conhecendo o Emacs

* [GNU Emacs #0 - Primeiros passos](https://youtu.be/oNmbldRJqZM)
* [GNU Emacs #1 - Configurações básicas da interface](https://youtu.be/0Le151jS57o)
* [GNU Emacs #2 - Configurando e expandindo o Emacs](https://youtu.be/UCJwchTEWeU)

